<?php
/**
 * @file
 * Implement Views hooks for views_user_login_link.module.
 */

/**
 * Implements hook_views_data().
 */
function views_user_login_link_views_data() {
  $data = array();
  
  $data['users']['views_user_login_link'] = array(
    'field' => array(
      'title' => t('Login link'),
      'help' => t('Output a onetime-login-link.'),
      'handler' => 'views_user_login_link_field',
    ),
  );
  
  return $data;
}
