<?php
/**
 * @file
 * Implement Views field handler for views_user_login_link.module.
 */

/**
 * A handler to provide a field containing a onetime-login-link.
 *
 * @ingroup views_field_handlers
 */
class views_user_login_link_field extends views_handler_field {
  
  function construct() {
    parent::construct();
    $this->additional_fields['uid'] = 'uid';
    $this->additional_fields['pass'] = 'pass';
    $this->additional_fields['login'] = 'login';
  }

  // An example of field level access control.
  function access() {
    return user_access('administer users');
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $account = new stdClass();
    $account->uid = $this->get_value($values, 'uid');
    $account->pass = $this->get_value($values, 'pass');
    $account->login = $this->get_value($values, 'login');

    return user_pass_reset_url($account);
  }
}
