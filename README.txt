Views User Login Link
=====================

This module provides a [Views][] plugin to display a one-time-login URL or link
for a user in the context of a View.

[Views]: http://drupal.org/project/views

It was developed to help site owners communicate with the users of sites which
have been redeveloped on Drupal, allowing them to send "we have a new site,
click to login" emails to their user base.
